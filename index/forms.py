from django import forms

class formKegiatan(forms.Form):
        nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Apa kegiatan favortimu?',
            'type' : 'text',
            'required' : True,
        }))

class formOrang(forms.Form):
        nama_orang = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Siapa nama kamu?',
            'type' : 'text',
            'required' : True,
        }))