from django.test import TestCase, Client
from . import models
from .views import tambah
from .models import Kegiatan, Orang
from http import HTTPStatus
from .views import tambah
from .forms import formKegiatan, formOrang
from .apps import IndexConfig
from django.apps import apps

# Create your tests here.
class UnitTestStory6(TestCase):
    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_daftar_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan = "main balon")
        Orang.objects.create(nama_orang = "kusuma")
        counter = Kegiatan.objects.all().count()
        counter += Orang.objects.all().count()
        self.assertEqual(counter, 2)

    def test_model_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan = "ppw")
        kegiatan  = Kegiatan.objects.get(nama_kegiatan = "ppw")
        self.assertEqual(str(kegiatan), "ppw")
    
    def test_model_orang(self):
        Orang.objects.create(nama_orang = "krox")
        orang  = Orang.objects.get(nama_orang = "krox")
        self.assertEqual(str(orang), "krox")

    
    def test_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'form.html')
        
    def test_form_valid(self):
        data = {'nama_kegiatan':"ngoding"}
        kegiatan_form = formKegiatan(data=data)
        self.assertTrue(kegiatan_form.is_valid())
        self.assertEqual(kegiatan_form.cleaned_data['nama_kegiatan'],"ngoding")

    def test_form_POST(self):
        test_str = 'xyz'
        response_post = Client().post('', {'nama_kegiatan':"xyz"})
        self.assertEqual(response_post.status_code,200)
        kegiatan_form = formKegiatan(data={'nama_kegiatan':test_str})
        self.assertTrue(kegiatan_form.is_valid())
        self.assertEqual(kegiatan_form.cleaned_data['nama_kegiatan'],"xyz")

    def test_apps(self):
        self.assertEqual(IndexConfig.name, 'index')
        self.assertEqual(apps.get_app_config('index').name, 'index')



    

