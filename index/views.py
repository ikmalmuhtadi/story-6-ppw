from django.shortcuts import render
from . import forms, models
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@csrf_exempt

def tambah(request):
    if request.method == "POST":
        if 'nama_kegiatan' in request.POST:
            formKegiatan = forms.formKegiatan(request.POST)
            if formKegiatan.is_valid():
                context = models.Kegiatan()
                context.nama_kegiatan = formKegiatan.cleaned_data['nama_kegiatan']
                context.save()
        elif 'nama_orang' in request.POST and 'id_kegiatan' in request.POST:
            formOrang = forms.formOrang(request.POST)
            if formOrang.is_valid():
                models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])
                context = models.Orang()
                context.nama_orang = formOrang.cleaned_data['nama_orang']
                context.kegiatan = models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])
                context.save()
    

    lst_kegiatan = models.Kegiatan.objects.all()
    data_lengkap = []
    for kegiatan in lst_kegiatan:
        orang = models.Orang.objects.filter(kegiatan = kegiatan)
        lst_peserta = []
        for i in orang:
            lst_peserta.append(i)
        data_lengkap.append((kegiatan, lst_peserta))
    
    context_dict = {
        'form_kegiatan' : forms.formKegiatan,
        'form_orang' : forms.formOrang,
        'kegiatan'  : data_lengkap
    }
    
    return render(request, 'form.html', context_dict)
