from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length = 64, default = "")
    
    def __str__(self):
        return self.nama_kegiatan



class Orang(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete= models.CASCADE, null=True, blank=True)
    nama_orang = models.CharField(max_length = 64)

    def __str__(self):
        return self.nama_orang
